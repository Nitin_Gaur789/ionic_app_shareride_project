(self["webpackChunkloginform"] = self["webpackChunkloginform"] || []).push([["src_app_pages_loginscreen_loginscreen_module_ts"],{

/***/ 6739:
/*!*****************************************************************!*\
  !*** ./src/app/pages/loginscreen/loginscreen-routing.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginscreenPageRoutingModule": () => (/* binding */ LoginscreenPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _loginscreen_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loginscreen.page */ 5262);




const routes = [
    {
        path: '',
        component: _loginscreen_page__WEBPACK_IMPORTED_MODULE_0__.LoginscreenPage
    }
];
let LoginscreenPageRoutingModule = class LoginscreenPageRoutingModule {
};
LoginscreenPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginscreenPageRoutingModule);



/***/ }),

/***/ 2586:
/*!*********************************************************!*\
  !*** ./src/app/pages/loginscreen/loginscreen.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _loginscreen_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loginscreen.page */ 5262);
/* harmony import */ var _loginscreen_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loginscreen-routing.module */ 6739);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _loginscreen_routing_module__WEBPACK_IMPORTED_MODULE_1__.LoginscreenPageRoutingModule
        ],
        declarations: [_loginscreen_page__WEBPACK_IMPORTED_MODULE_0__.LoginscreenPage]
    })
], LoginPageModule);



/***/ }),

/***/ 5262:
/*!*******************************************************!*\
  !*** ./src/app/pages/loginscreen/loginscreen.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginscreenPage": () => (/* binding */ LoginscreenPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_loginscreen_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./loginscreen.page.html */ 2432);
/* harmony import */ var _loginscreen_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loginscreen.page.scss */ 6260);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let LoginscreenPage = class LoginscreenPage {
    constructor() { }
    ngOnInit() {
    }
};
LoginscreenPage.ctorParameters = () => [];
LoginscreenPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-loginscreen',
        template: _raw_loader_loginscreen_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_loginscreen_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginscreenPage);



/***/ }),

/***/ 6260:
/*!*********************************************************!*\
  !*** ./src/app/pages/loginscreen/loginscreen.page.scss ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-header {\n  position: absolute;\n}\n\nion-toolbar {\n  --ion-background-color: transparent;\n  --opacity: 0;\n}\n\nion-content {\n  --ion-background-color:#E1EAF9;\n}\n\n.wrapform {\n  margin-top: 15px;\n}\n\nion-item {\n  color: #74787E;\n  --border-color: #D407D8;\n  --highlight-color-focused:#647890;\n  --padding-start: 0;\n  --inner-padding-end: 0;\n  font-size: 18px;\n}\n\nion-item ion-input {\n  color: #647890;\n}\n\n.password_forgot {\n  text-align: right;\n}\n\n.password_forgot ion-button {\n  font-size: 12px;\n}\n\n.sign-btn-wrap {\n  text-align: center;\n  padding-top: 20px;\n}\n\n.sign-btn-wrap .sign-btn {\n  --ion-primary-color: #215AB7;\n  color: #F4F6FA;\n}\n\n.text-tag {\n  margin-top: 30px;\n  text-align: center;\n}\n\n.text-tag span {\n  font-size: 18px;\n  font-weight: normal;\n  color: #898585;\n}\n\n.social-btns {\n  margin-top: 25px;\n  text-align: center;\n}\n\n.social-btns ion-button {\n  width: 40px;\n  padding-left: 5px !important;\n}\n\nion-card {\n  --ion-background-color: transparent;\n  box-shadow: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luc2NyZWVuLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGtCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxtQ0FBQTtFQUNBLFlBQUE7QUFDRDs7QUFFQTtFQUNDLDhCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxnQkFBQTtBQUNEOztBQUVBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtBQUNBOztBQUFDO0VBQ0EsY0FBQTtBQUVEOztBQUVBO0VBQ0MsaUJBQUE7QUFDRDs7QUFBQztFQUNBLGVBQUE7QUFFRDs7QUFFQTtFQUNDLGtCQUFBO0VBQ0EsaUJBQUE7QUFDRDs7QUFBQztFQUNBLDRCQUFBO0VBQ0EsY0FBQTtBQUVEOztBQUVBO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtBQUNGOztBQUFFO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQUVGOztBQUVBO0VBQ0EsZ0JBQUE7RUFDRSxrQkFBQTtBQUNGOztBQUFFO0VBQ0EsV0FBQTtFQUNBLDRCQUFBO0FBRUY7O0FBRUE7RUFDQyxtQ0FBQTtFQUNBLDJCQUFBO0FBQ0QiLCJmaWxlIjoibG9naW5zY3JlZW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0fVxyXG5cclxuaW9uLXRvb2xiYXJ7XHJcblx0LS1pb24tYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcblx0LS1vcGFjaXR5OiAwO1xyXG59XHJcblxyXG5pb24tY29udGVudHtcclxuXHQtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNFMUVBRjk7XHJcbn1cclxuXHJcbi53cmFwZm9ybXtcclxuXHRtYXJnaW4tdG9wOiAxNXB4O1xyXG59XHJcblxyXG5pb24taXRlbXtcclxuY29sb3I6ICM3NDc4N0U7XHJcbi0tYm9yZGVyLWNvbG9yOiAjRDQwN0Q4O1xyXG4tLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiM2NDc4OTA7XHJcbi0tcGFkZGluZy1zdGFydDogMDtcclxuLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcclxuZm9udC1zaXplOiAxOHB4O1xyXG5cdGlvbi1pbnB1dHtcclxuXHRjb2xvcjogIzY0Nzg5MDtcclxuXHR9XHJcbn1cclxuXHJcbi5wYXNzd29yZF9mb3Jnb3R7XHJcblx0dGV4dC1hbGlnbjogcmlnaHQ7XHJcblx0aW9uLWJ1dHRvbntcclxuXHRmb250LXNpemU6IDEycHg7XHJcblx0fVxyXG59XHJcblxyXG4uc2lnbi1idG4td3JhcHtcclxuXHR0ZXh0LWFsaWduOmNlbnRlcjtcclxuXHRwYWRkaW5nLXRvcDogMjBweDtcclxuXHQuc2lnbi1idG57XHJcblx0LS1pb24tcHJpbWFyeS1jb2xvcjogIzIxNUFCNztcclxuXHRjb2xvcjojRjRGNkZBO1xyXG5cdH1cclxufVxyXG5cclxuLnRleHQtdGFne1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHNwYW57XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgY29sb3I6ICM4OTg1ODU7XHJcbiAgfVxyXG59XHJcblxyXG4uc29jaWFsLWJ0bnN7XHJcbm1hcmdpbi10b3A6IDI1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGlvbi1idXR0b257XHJcbiAgd2lkdGg6NDBweDtcclxuICBwYWRkaW5nLWxlZnQ6IDVweCAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG5cclxuaW9uLWNhcmR7XHJcblx0LS1pb24tYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcblx0Ym94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG59Il19 */");

/***/ }),

/***/ 2432:
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/loginscreen/loginscreen.page.html ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border header_nitin\">\n  <ion-toolbar >\n   <!-- <ion-title>loginscreen</ion-title> -->\n  </ion-toolbar>\n  <ion-buttons slot=\"start\">\n    <ion-back-button defaulthref=\"loogin\" text=\"\"></ion-back-button>\n  </ion-buttons>\n</ion-header>\n\n<ion-content>\n  <ion-card class=\"ion-no-margin\">\n    <ion-img src=\"assets/Images/header1.png\"></ion-img>\n\n  </ion-card>\n\n  <ion-card-content>\n    <div class=\"wrapform\">\n      <ion-item>\n        <ion-input type=\"text\" name=\"email\" [(ngModel)]=\"email\" placeholder=\"Email\" required></ion-input>\n      <ion-icon name=\"mail-outline\"></ion-icon>\n      </ion-item>\n      <ion-item>\n        <ion-input type=\"password\" name=\"password\" [(ngModel)]=\"password\" placeholder=\"**********\" required></ion-input>\n       <ion-icon name=\"lock-closed-outline\"></ion-icon>\n      </ion-item>\n    </div>\n    <div class=\"password_forgot\">\n      <ion-button fill=\"clear\" class=\"ion-no-padding\">Forgot Password?</ion-button>\n    </div>\n    <div class=\"sign-btn-wrap\">\n<ion-button shape=\"round\" fill=\"solid\" class=\"sign-btn\">Login</ion-button>\n    </div>\n\n    <div class=\"text-tag\">\n      <span>\n        Join US On Social Media\n      </span>\n    </div>\n\n       <div class=\"social-btns\">\n    <ion-button fill=\"clear\" class=\"facebookloginbtn ion-no-padding\">\n    <ion-img src=\"assets/Images/facebook.png\"></ion-img>\n    </ion-button>\n     <ion-button fill=\"clear\" class=\"googleloginbtn ion-no-padding\">\n    <ion-img src=\"assets/Images/google.png\"></ion-img>\n    </ion-button>\n     <ion-button fill=\"clear\" class=\"appleloginbtn ion-no-padding\">\n    <ion-img src=\"assets/Images/apple.png\"></ion-img>\n    </ion-button>\n     </div>\n  </ion-card-content>\n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_loginscreen_loginscreen_module_ts.js.map